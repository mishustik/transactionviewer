package com.example.mikhailoparin.badootestapp;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Currency;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Locale;

public class Products extends AppCompatActivity {

    public static final String TRANSACTIONS_FILE = "transactions.json";
    public static final String RATES_FILE = "rates.json";

    enum State {Visited, Unvisited, Visiting};
    HashMap<String, Integer> currencies = new HashMap<>();
    double[][] rates;

    class Transaction{
        private String name;
        private String currency;
        private double actualAmount;

        public Transaction(String name, String currency, double actualAmount){
            this.name = name;
            this.actualAmount = actualAmount;
            this.currency = currency;
        }

        public String getName(){
            return name;
        }

        public String getCurrency(){
            return currency;
        }

        public double getActualAmount(){
            return actualAmount;
        }
    }

    public String loadJSONFromAsset(String fileName) {
        String json = null;
        try {
            InputStream is = this.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public double calculateConvertedAmount(String currency, double actualAmount){

        return actualAmount * rates[currencies.get(currency)][currencies.get("GBP")];
    }

    public HashMap<String, ArrayList<Transaction>> createTransactionsFromJSONFile(String filename){

        HashMap<String, ArrayList<Transaction>> data = new HashMap<>();
        ArrayList<Transaction> list;

        try {
            JSONArray array = new JSONArray(loadJSONFromAsset(filename));

            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);
                Double amount = obj.getDouble("amount");
                String sku = obj.getString("sku");
                String currency = obj.getString("currency");

                if (!currencies.containsKey(currency)) {
                    currencies.put(currency, currencies.size());
                }

                if (!data.containsKey(sku)) {
                    list = new ArrayList<>();
                    data.put(sku, list);
                } else {
                    list = data.get(sku);
                }

                String currencyCode = Currency.getInstance(currency).getSymbol();
                StringBuilder name = new StringBuilder(currencyCode);
                name.append(amount);

                Transaction trans = new Transaction(name.toString(), currency, amount);
                list.add(trans);
            }
        } catch (JSONException e){
            e.printStackTrace();
        }

        return data;
    }

    public ArrayList<HashMap<String, String>> buildListOfProducts(HashMap<String, ArrayList<Transaction>> data){
        final ArrayList<HashMap<String, String>> products = new ArrayList<>();
        HashMap<String, String> item;
        for (String s : data.keySet()){
            item = new HashMap<>();
            item.put("sku", s);
            item.put("transactions", String.valueOf(data.get(s).size()) + " transactions");
            products.add(item);
        }

        return products;
    }

    public ArrayList<HashMap<String, String>> buildListOfTransaction(ArrayList<Transaction> transactions){
        ArrayList<HashMap<String, String>> transList = new ArrayList<>();
        HashMap<String, String> transEl;

        DecimalFormat formatter = new DecimalFormat("#.##", DecimalFormatSymbols.getInstance( Locale.ENGLISH ));
        formatter.setRoundingMode( RoundingMode.DOWN );
        double total = 0.0;
        for (Transaction tr : transactions){
            transEl = new HashMap<>();
            transEl.put("transaction", tr.getName());

            double convertedAmount = calculateConvertedAmount(tr.getCurrency(), tr.getActualAmount());
            total += convertedAmount;

            transEl.put("convertedAmount", "£" + formatter.format(convertedAmount));
            transList.add(transEl);
        }
        transEl = new HashMap<>();
        transEl.put("total", formatter.format(total));
        transList.add(transEl);

        return transList;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);

        final HashMap<String, ArrayList<Transaction>> data = createTransactionsFromJSONFile(TRANSACTIONS_FILE);

        CurrencyGraph graph = buildGraphFromRates(RATES_FILE);

        rates = buildRateTable(graph);

        final ArrayList<HashMap<String, String>> products = buildListOfProducts(data);
        ListAdapter adapter = new SimpleAdapter(Products.this, products,
                R.layout.product_item,
                new String[] { "sku", "transactions" }, new int[] {
                R.id.sku,R.id.transactions});

        ListView lv = (ListView) findViewById(R.id.listProducts);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                ArrayList<Transaction> transactions = data.get(products.get(position).get("sku"));
                ArrayList<HashMap<String, String>> transList = buildListOfTransaction(transactions);

                Intent i = new Intent(getApplicationContext(), Transactions.class);
                i.putExtra("transactions",transList);
                startActivity(i);
            }
        });
    }

    public double[][] buildRateTable(CurrencyGraph graph){
        int size = currencies.size();
        double[][] rates = new double[size][size];

        for (String start : currencies.keySet()){
            for (String end : currencies.keySet()){
                GraphNode startNode = graph.getMap().get(start);
                GraphNode endNode = graph.getMap().get(end);
                rates[currencies.get(start)][currencies.get(end)] = bfs(graph, startNode, endNode);
                rates[currencies.get(end)][currencies.get(start)] = 1 / rates[currencies.get(start)][currencies.get(end)];
            }
        }

        return rates;
    }

    public double bfs(CurrencyGraph graph, GraphNode start, GraphNode end){
        if (start == end){
            return 1.0;
        }

        LinkedList<GraphNode> queue = new LinkedList<>();

        for (GraphNode node : graph.getNodes()){
            node.setState(State.Unvisited);
            node.setFactor(1.0);
        }

        start.setState(State.Visiting);
        queue.add(start);

        GraphNode u;
        while (!queue.isEmpty()){
            u = queue.removeFirst();
            if (u != null){
                for (WeightedGraphNode v : u.getNeighbors()){
                    if (v.getNode().getState() == State.Unvisited){
                        if (v.getNode() == end){
                            return u.getFactor() * v.getWeight();
                        } else {
                            v.getNode().setFactor(u.getFactor() * v. getWeight());
                            v.getNode().setState(State.Visiting);
                            queue.add(v.getNode());
                        }
                    }
                }
                u.setState(State.Visited);
            }
        }

        return -1;
    }

    public CurrencyGraph buildGraphFromRates(String fileName){

        CurrencyGraph graph = new CurrencyGraph();
        for (String node : currencies.keySet()){
            graph.getOrCreateNode(node);
        }

        try {
            JSONArray array = new JSONArray(loadJSONFromAsset(fileName));
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);
                String from = obj.getString("from");
                String to = obj.getString("to");
                Double rate = obj.getDouble("rate");

                graph.addEdge(from, to, rate);
            }
        } catch (JSONException e){
            e.printStackTrace();
        }

        return graph;
    }

    class CurrencyGraph{
        private ArrayList<GraphNode> nodes = new ArrayList<>();
        private HashMap<String, GraphNode> map = new HashMap<>();

        public GraphNode getOrCreateNode(String name){
            if (!map.containsKey(name)){
                GraphNode node = new GraphNode(name);
                nodes.add(node);
                map.put(name, node);
            }

            return map.get(name);
        }

        public void addEdge(String first, String second, double weight){
            GraphNode start = getOrCreateNode(first);
            GraphNode end = getOrCreateNode(second);
            start.addNeighbor(end, weight);
            end.addNeighbor(start, 1 / weight);
        }

        public ArrayList<GraphNode> getNodes(){
            return  nodes;
        }

        public HashMap<String, GraphNode> getMap(){
            return  map;
        }
    }

    class WeightedGraphNode{
        private double weight;
        private GraphNode node;

        public WeightedGraphNode(GraphNode node, double weight){
            this.node = node;
            this.weight = weight;
        }

        public GraphNode getNode(){
            return node;
        }

        public double getWeight(){
            return weight;
        }
    }

    class GraphNode{
        private ArrayList<WeightedGraphNode> neighbors = new ArrayList<>();
        private HashMap<String, GraphNode> map = new HashMap<>();
        private String name;
        private State state;
        private double factor;

        public GraphNode(String name){
            this.name = name;
            state = State.Unvisited;
            factor = 1.0;
        }

        public void addNeighbor(GraphNode node, double weight){
            if (!map.containsKey(node.getName())){
                neighbors.add(new WeightedGraphNode(node, weight));
                map.put(node.getName(), node);
            }
        }

        public String getName(){
            return name;
        }

        public State getState(){
            return state;
        }

        public void setState(State state){
            this.state = state;
        }

        public double getFactor(){
            return factor;
        }

        public void setFactor(double factor){
            this.factor = factor;
        }

        public ArrayList<WeightedGraphNode> getNeighbors(){
            return neighbors;
        }
    }
}
