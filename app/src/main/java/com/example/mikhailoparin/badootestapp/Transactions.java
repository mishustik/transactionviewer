package com.example.mikhailoparin.badootestapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by mikhailsoparin on 03/09/16.
 */
public class Transactions extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transactions);

        Intent intent = getIntent();
        ArrayList<HashMap<String, String>> transactions = (ArrayList<HashMap<String, String>>) intent.getSerializableExtra("transactions");
        String total = transactions.remove(transactions.size() - 1).get("total");

        TextView tv = (TextView) findViewById(R.id.total);
        tv.setText("Total: £" + total);

        ListAdapter adapter = new SimpleAdapter(Transactions.this, transactions,
                R.layout.product_item,
                new String[] { "transaction", "convertedAmount" }, new int[] {
                R.id.sku,R.id.transactions});

        ListView lv = (ListView) findViewById(R.id.listTransactions);
        lv.setAdapter(adapter);
    }
}