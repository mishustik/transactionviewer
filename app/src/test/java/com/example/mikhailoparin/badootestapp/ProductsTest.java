package com.example.mikhailoparin.badootestapp;

import junit.framework.TestCase;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.Assert.*;

/**
 * Created by mikhailsoparin on 03/09/16.
 */
public class ProductsTest extends TestCase{

    Products products;

    public void setUp() throws Exception{
        super.setUp();
        products = new Products();
    }

    @Test
    public void testInitializeRates() throws Exception {

        products.initializeRates();
        for (int i = 0; i < products.rates.length; i++){
            for (int j = 0; j < products.rates[0].length / 2; j++){
                assertEquals(products.rates[i][j],products.rates[i][products.rates.length - j - 1]);
            }
        }
    }

}