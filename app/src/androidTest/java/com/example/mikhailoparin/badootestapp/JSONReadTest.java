package com.example.mikhailoparin.badootestapp;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertNotEquals;

/**
 * Created by mikhailsoparin on 04/09/16.
 */

@RunWith(AndroidJUnit4.class)
public class JSONReadTest {

    @Rule
    public ActivityTestRule<Products> productsActivityTestRule = new ActivityTestRule<Products>(Products.class);

    @Test
    public void readJSONFile_checkMatrix() throws Exception{
        Products products = productsActivityTestRule.getActivity();

        products.initializeRates();
        products.fillRatesFromJSONFile(Products.RATES_FILE);
        products.runRatesAlgorithm();
        for (int i = 0; i < products.rates.length; i++){
            for (int j = 0; j < products.rates[0].length; j++){
                assertNotEquals(0.0,products.rates[i][j]);
            }
        }
    }

}
