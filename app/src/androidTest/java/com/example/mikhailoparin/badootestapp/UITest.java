package com.example.mikhailoparin.badootestapp;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.pressBack;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by mikhailsoparin on 03/09/16.
 */

@RunWith(AndroidJUnit4.class)
public class UITest {

    @Rule
    public ActivityTestRule<Products> productsActivityTestRule = new ActivityTestRule<Products>(Products.class);

    @Test
    public void clickListViewItemButton_openTransactionsUi() throws Exception{
        onView(withId(R.id.listProducts))
                .perform(click());
        onView(withId(R.id.listTransactions))
                .check(matches(isDisplayed()));
        onView(withId(R.id.total))
                .check(matches(isDisplayed()));

        onView(withId(R.id.listTransactions))
                .perform(pressBack());
        onView(withId(R.id.listProducts))
                .check(matches(isDisplayed()));
    }

}
